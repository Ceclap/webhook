import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';

export const swaggerPath = 'swagger';
export function setupSwagger(
  app: INestApplication,
  path: string = swaggerPath,
): void {
  const config = new DocumentBuilder()
    .setTitle('Webhooks')
    .setDescription('Docs of Webhooks API')
    .setVersion('1.0')
    .build() as OpenAPIObject;
  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup(path, app, document);
}
