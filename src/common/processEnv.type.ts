export const NODE_ENV = [
  'local',
  'test',
  'development',
  'staging',
  'production',
] as const;
export type NodeEnv = (typeof NODE_ENV)[number];

export interface ProcessEnv{
  NODE_ENV: NodeEnv;
  APP_PORT: number;
  APP_CORS_ORIGIN: string;
  APP_GLOBAL_PREFIX: string;

  // JWT environment variables
  JWT_SECRET: string;
  JWT_EXPIRES_IN: string;

  // Postgres environment variables
  POSTGRES_HOST: string;
  POSTGRES_PORT: number;
  POSTGRES_DATABASE: string;
  POSTGRES_USERNAME: string;
  POSTGRES_PASSWORD: string;
  POSTGRES_SCHEMA: string;
}