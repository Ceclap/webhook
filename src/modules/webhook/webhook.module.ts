import { Module } from '@nestjs/common';
import { WebhookController } from './webhook.controller';
import { WebhookService } from './webhook.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Webhook } from "@core/database/entity/webhook.entity";

@Module({
  imports:[
    TypeOrmModule.forFeature([Webhook]),
  ],
  controllers: [WebhookController],
  providers: [WebhookService]
})
export class WebhookModule {}
