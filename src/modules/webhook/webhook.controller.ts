import { Controller, Post } from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { WebhookService } from "@modules/webhook/webhook.service";

@ApiTags('webhook')
@Controller('webhook')
export class WebhookController {

  constructor(
    private readonly webhookService: WebhookService
  ) {
  }

  @ApiResponse({
    status: 200,
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            message: { type: 'string', example: 'success' },
          }
        }
      }
    }
  })
  @Post()
  async webhook() {
    return this.webhookService.post()
  }
}
