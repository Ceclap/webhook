import { Module } from '@nestjs/common';
import { ConfigModule } from "@nestjs/config";
import { configModuleOptions } from "@core/config/config-module-options";
import { WebhookModule } from './webhook/webhook.module';

@Module({
  imports: [
    ConfigModule.forRoot(configModuleOptions),
    WebhookModule
  ],
})
export class AppModule {}
