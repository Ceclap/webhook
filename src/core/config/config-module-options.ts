import { postgresConfig } from '@core/database/postgres.config';
import { ConfigModuleOptions } from '@nestjs/config';

import { appConfig } from './app.config';

export const configModuleOptions: ConfigModuleOptions = {
  isGlobal: true,
  load: [appConfig, postgresConfig],
};
