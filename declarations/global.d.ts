import { ProcessEnv as ProcessEnvType } from '@common/processEnv.type';

export declare global {
  namespace NodeJS {
    interface ProcessEnv extends ProcessEnvType {};
  }
}
