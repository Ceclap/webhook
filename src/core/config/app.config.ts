import { NodeEnv } from '@common/processEnv.type';
import { registerAs } from '@nestjs/config';

export interface AppConfig {
  env: NodeEnv;
  port: number;
  globalPrefix: string;
  cors: {
    origin: string;
  };
}

export const appConfig = registerAs('app', (): AppConfig => {
  return {
    env: process.env.NODE_ENV,
    port: 3000,
    globalPrefix: process.env.APP_GLOBAL_PREFIX,
    cors: {
      origin: process.env.APP_CORS_ORIGIN,
    },
  };
});
